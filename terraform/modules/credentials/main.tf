# RDS Password
resource "random_string" "postgress_password" {
  keepers = {
    # Generate a new password every time this module is run
    build = timestamp()
  }

  length           = 32
  special          = true
  override_special = "!#$%^&*()"
}

# KMS used to encrypt database user and password, and the database itself
resource "aws_kms_key" "postgres_kms" {
  description         = "${var.resource_prefix} postgres encryption key"
  enable_key_rotation = true

  tags = {
    TerraformStack = var.resource_prefix
  }
}

# Set up secret manager 
resource "aws_secretsmanager_secret" "postgres_secret" {
  name = "${var.resource_prefix}-secret"
}

# Define the secret as a map
locals {
  secret = {
    database_username = var.database_username
    database_password = random_string.postgress_password.result
  }
}

# Store secret 
resource "aws_secretsmanager_secret_version" "postgres_secret_version" {
  secret_id     = aws_secretsmanager_secret.postgres_secret.id
  secret_string = jsonencode(local.secret)
}
