output "username" {
  value = var.database_username
}

output "password" {
  value = random_string.postgress_password.result
}

output "kms_arn" {
  value = aws_kms_key.postgres_kms.arn
}
