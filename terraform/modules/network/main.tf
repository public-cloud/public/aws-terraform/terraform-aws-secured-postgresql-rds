data "aws_availability_zones" "available_availability_zones" {
  state = "available"
}

locals {
  allow_connection_to_office = var.office_cidr_range != "0.0.0.0/32"
  ip_range_as_list           = split(".", data.aws_vpc.postgres_vpc.cidr_block)
}

data "aws_vpc" "postgres_vpc" {
  default = true
}

data "aws_subnets" "db_subnets" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.postgres_vpc.id]
  }
}

resource "aws_db_subnet_group" "postgres_subnet_group" {
  name = "${var.resource_prefix}subnet_group"
  #subnet_ids = data.aws_subnets.db_subnets.*.id
  subnet_ids = data.aws_subnets.db_subnets.ids
  tags = {
    Name           = "${var.resource_prefix} subnet group"
    Environment    = var.environment
    TerraformStack = var.resource_prefix
  }
}

data "aws_ec2_managed_prefix_list" "UW_CIDR_blocks" {
  filter {
    name   = "prefix-list-name"
    values = ["UW-Madison"]
  }
}

resource "aws_security_group" "postgres_security_group" {
  vpc_id      = data.aws_vpc.postgres_vpc.id
  name        = "${var.resource_prefix} security group"
  description = "postgres security group"
  ingress {
    prefix_list_ids = [data.aws_ec2_managed_prefix_list.UW_CIDR_blocks.id]
    from_port       = var.database_port
    to_port         = var.database_port
    protocol        = "tcp"
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name           = "${var.resource_prefix}-security-group"
    Environment    = var.environment
    TerraformStack = var.resource_prefix
  }
}
