

data "aws_availability_zones" "available_availability_zones" {
  state = "available"
}
# Make a list of avilability zones for this region
locals {
  az_list                = data.aws_availability_zones.available_availability_zones.names
  prefix_list_definition = jsondecode(file("./data/ip_prefix_list.json"))
}

# Get the default subnets (one per availability zone)
resource "aws_default_subnet" "default_subnet" {
  for_each          = toset(local.az_list)
  availability_zone = each.value
  #force_destroy = true
}

# Get the default VPC
resource "aws_default_vpc" "default_vpc" {
  #force_destroy = true
}

# Create a prefix list with approved IPs
resource "aws_ec2_managed_prefix_list" "organization_prefix_list" {
  name           = local.prefix_list_definition.prefix_list_name
  address_family = local.prefix_list_definition.address_family
  max_entries    = local.prefix_list_definition.max_entries

}

# Add entries to the prefix list
# Prefix list has concurrancy issues use terraform apply -parallelism=1 
# as a workaround until aws fixes the root cause
# https://github.com/hashicorp/terraform-provider-aws/issues/21835

resource "aws_ec2_managed_prefix_list_entry" "organization_prefix_list_entry" {
  for_each       = tomap(local.prefix_list_definition.cidr_list)
  cidr           = each.value
  description    = each.key
  prefix_list_id = aws_ec2_managed_prefix_list.organization_prefix_list.id
}

# Close ports the default security group
resource "aws_default_security_group" "default_security_groups" {
  vpc_id = aws_default_vpc.default_vpc.id

  tags = {
    Name = "Default_SecurityGroup"
  }
}


###security_groups with the aws_ec2_managed_prefix_list attached
resource "aws_security_group" "uw_ssh_allowed_list" {
  vpc_id      = aws_default_vpc.default_vpc.id
  name        = "uw_ssh_allowed_list"
  description = "Created SG for SSH ${local.prefix_list_definition.prefix_list_name} IP ranges"

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    prefix_list_ids = [aws_ec2_managed_prefix_list.organization_prefix_list.id]
    description     = "Created SG for SSH ${local.prefix_list_definition.prefix_list_name} IP ranges"

  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "uw_ssh_allowed_list"
  }
}

resource "aws_security_group" "uw_rdp_allowed_list" {
  name        = "uw_rdp_allowed_list"
  description = "Created SG for RDP ${local.prefix_list_definition.prefix_list_name} IP ranges"
  vpc_id      = aws_default_vpc.default_vpc.id

  ingress {
    from_port       = 3389
    to_port         = 3389
    protocol        = "tcp"
    prefix_list_ids = [aws_ec2_managed_prefix_list.organization_prefix_list.id]
    description     = "Created SG for RDP ${local.prefix_list_definition.prefix_list_name} IP ranges"
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "uw_rdp_allowed_list"
  }
}
