# PostgreSQL database 

## What it is 
This creates a secret manager, KMS, Network security group, RDS instance, and cloudwatch alerts for monitoring. It uses the base line VPC and subnets as well as allowing access to the UW Madison prefix list.  

## How to use

1. You will need terrafrom installed
2. Run 'aws configure' with the account credentials you want to create this infrastructure in
3. the variables.tf in the main terraform folder some important ones to change are below   
    &emsp; instance_name - You can change this to whatever you like  
    &emsp; multi_availability_zone - if true it will be deploying to 3 AZs  
    &emsp; allocated_storage - amount of storage in DB  
4. The database name for connecting is just postgres
5. The password will be in your aws secrets manager 
6. The database enpoint URL is found by looking at he VPC in the AWS console